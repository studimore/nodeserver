# Studimore Microservices

The following steps will guide you through the process of setting up our backend repo `Microservices` along with `Docker` to run the dev environement in your local device or the cloud.

## Development workflow

From root directory run:

```
docker-compose up
cd api-feed
npm i
npm run dev
```

### Prerequisites

The following tools need to be installed on your machine:

- [Docker](https://www.docker.com/products/docker-desktop)
- [AWS CLI](https://aws.amazon.com/cli/)

Furthermore, you need to have:

- an [Amazon Web Services](https://console.aws.amazon.com) account
- a [DockerHub](https://hub.docker.com/) account

### Create an S3 bucket

The application uses an S3 bucket to store the images so an AWS S3 Bucket needs to be created

#### Permissions

Save the following policy in the Bucket policy editor:

```JSON
{
 "Version": "2012-10-17",
 "Id": "Policy1565786082197",
 "Statement": [
 {
 "Sid": "Stmt1565786073670",
 "Effect": "Allow",
 "Principal": {
 "AWS": "__YOUR_USER_ARN__"
 },
 "Action": [
 "s3:GetObject",
 "s3:PutObject"
 ],
 "Resource": "__YOUR_BUCKET_ARN__/*"
 }
 ]
}
```

Modify the variables `__YOUR_USER_ARN__` and `__YOUR_BUCKET_ARN__` by your own data.

#### CORS configuration

Save the following configuration in the CORS configuration Editor:

```XML
<?xml version="1.0" encoding="UTF-8"?>
 <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
 <CORSRule>
 <AllowedOrigin>*</AllowedOrigin>
 <AllowedMethod>GET</AllowedMethod>
 <AllowedMethod>POST</AllowedMethod>
 <AllowedMethod>DELETE</AllowedMethod>
 <AllowedMethod>PUT</AllowedMethod>
 <MaxAgeSeconds>3000</MaxAgeSeconds>
 <AllowedHeader>Authorization</AllowedHeader>
 <AllowedHeader>Content-Type</AllowedHeader>
 </CORSRule>
</CORSConfiguration>
```

## Deploy on local

`Docker` is used to start the application on the local environment

The variables below need to be added to your environment:

```
POSTGRESS_USERNAME=udagram
POSTGRESS_PASSWORD=local
POSTGRESS_DB=udagram
POSTGRESS_HOST=db
WT_SECRET=mySecret
AWS_BUCKET=__YOUR_AWS_BUCKET_NAME__
AWS_REGION=__YOUR_AWS_BUCKET_REGION__
AWS_PROFILE=__YOUR_AWS_PROFILE__
```

Replace the variables `__YOUR_AWS_BUCKET_NAME__`, `__YOUR_AWS_BUCKET_REGION__` and `__YOUR_AWS_PROFILE__` by your own information

Build the images by running:

```
docker-compose -f docker-compose-build.yaml build --parallel
```

Start the application and services:

```
docker-compose up
```

The application is now running at http://localhost:8100

## Setting up DB user and managing access

Run the following command. You need to make sure docker is running. This will open a bash shell that allows you to interact with the postgresql running on one of the docker containers.

```
  docker exec -it docker_db_1 /bin/sh
```

Once the bash shell is open run the following postgres queries

```
  psql -U postgres;

  create database studimoredev;

  create role studimoredev with login password 'class2020';
```

## Migrating to Microservices

Migrating to microservices from the current monolithic setup means being able to run individual services from it's own repo rather than all services running at once from the base repo. This means being able to run
'npm run dev' from individual service repos. You can follow the following steps to migrate to a microservices setup.

1. create services repos in the base repo. e.g. `api-user` and `api-feed` can be the different folders we create inside our base repo.

2. Each services repo will have it's own configuration file. The following files do not need to be customized based on the services so go ahead and copy/paste the following files into each of your services repos. e.g. `api-user/package.json` and `api-feed/package.json` and so on.

   - package.json
   - tsconfig.json
   - tslint.json
   - Dockerfile
   - .gitignore
   - .dockerignore
   - .npmrc

   we won't be running any npm commands from our base repo now that the services are broken down into it's own repo. So we can now delete all of the above files from our base repo.

3. Create a `/src` folder inside each base services repo. e.g. `api-user/src` and `api-feed/src`

4. each `services/src` will require some common files to configure aws, sequelize and to run the server. Again these will not change based on differnt services so go ahead and move these files to individual services. So simply copy/paste from base repo.

   the following files can be found in location `_your_base_repo_name/src`

   - aws.ts
   - sequelize.ts
   - server.ts

   move these files to each `_your_each_individual_service_repo/src` location. e.g. `api-user/src` and `api-feed/src` each will have a copy of these files such that `api-user/src/aws.ts` and `api-feed/src/aws.ts` and so on.

5. Migrate MVC files from base repo to it's own services repo. All MVC files will reside in the `/src` folder inside your services repo. follow the following sub-steps to complete this step.

   - create their own MVC folders: `/controllers`, `/routes` and `/models` in each services repo. e.g. `api-user/controllers`
   - migrate respective files to it's own services repo. e.g. `feed.controller.js` should be moved to `api-feed/controllers` and `user.controller.js` should be moved to `api-user/controllers` and so on.

   - each services repo will also require it's own `index.router.ts` and `index.model.ts`. So copy these files into each `/routes` and `/models` repo inside your services repo respectively.

     You'll need to EDIT these files so they only refer to files related to it's own individual services.

6. Migrate respective files in the `/seeders` folder in the base repo to a `/seeders` folder inside it's own services.

7. Once all files are migrated. You might need to edit `path-references` across all files since file locations have changed.

To see if the migration was successful run `npm-run-dev` from each services repo and it should run services only associted with the current repo. If you get errors follow error messages to correct any mistakes, it often will be wrongly refernced files.

## Database Seeding

All the seeder files are in the seeders folder. You need to make sure you have the sequelize-cli installed before proceeding further.

run this command to install sequelize in your local computer

```
npm install --save-dev sequelize-cli
```

you can also run the following command for further info regarding sequelize-cli

```
npx sequelize --help
```

Before running the seeder files you have to make sure both the datbase server and the node server are running. Find instructions above.

- To run all the seed files

```
sequelize db:seed:all
```

- to remove added data from database

```
sequelize db:seed:undo: all
```