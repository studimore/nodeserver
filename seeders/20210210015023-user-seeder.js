let accounts = require("./dummydata/accounts.js");
let profiles = require("./dummydata/profiles.js");
let communities = require("./dummydata/communities.js");
let managers = require("./dummydata/managers.js");
let students = require("./dummydata/students.js");

("use strict");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    //insert accounts objects to "Accounts" table and return IDs of inserted entries
    let accountIds = await queryInterface.bulkInsert("Accounts", accounts, {
      returning: ["id"],
    });

    //edit associated profile objects to reference the inserted account entries in "Accounts" table
    profiles.forEach((profile, index) => {
      const id = accountIds[index].id;
      profile["accountId"] = id;
    });

    //insert students, communities, ... objects to "Students" and "Communities", ... table
    await queryInterface.bulkInsert("Students", students);
    await queryInterface.bulkInsert("Communities", communities);
    await queryInterface.bulkInsert("Managers", managers);

    await queryInterface.bulkInsert("Profiles", profiles);
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("Profiles", null, {});
    await queryInterface.bulkDelete("Accounts", null, {});
  },
};
