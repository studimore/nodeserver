module.exports = [
  {
    email: "johndoe@me.com",
    phone: "1111111111",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "tannerphan@me.com",
    phone: "2222222222",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "listermatt@me.com",
    phone: "3333333333",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "drewstim@me.com",
    phone: "4444444444",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "doanjimmy@me.com",
    phone: "5555555555",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "ariascarlos@me.com",
    phone: "6666666666",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "mcdonaldpatrick@me.com",
    phone: "7777777777",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "boldingkevin@me.com",
    phone: "8888888888",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "hardenjames@me.com",
    phone: "9999999999",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    email: "jordanmichael@me.com",
    phone: "1212121212",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
