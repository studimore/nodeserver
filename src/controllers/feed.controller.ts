import { Router, Request, Response } from 'express';
import { FeedItems } from '../models/feed/FeedItems';
import { NextFunction } from 'connect';
import * as jwt from 'jsonwebtoken';
import * as AWS from '../aws';
import * as c from '../config/config';
import { Posts } from '../models/feed/Posts';
import { Medias } from '../models/feed/Medias';
import { V0MODELS } from '../models/model.index';

export function getAllFeedItems(req: Request, res: Response, next: NextFunction) {
    Posts.findAndCountAll({
        order: [['id', 'DESC']],
        // include:{ model: Medias, as: "Medias"}
    }).then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving feeds."
            });
        });
};

export function getFeedItem(req: Request, res: Response) {
    let { id } = req.params;
    Posts.findById(id).then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving feed"
            });
        });
};

export async function createFeed(req: Request, res: Response) {
    console.log(req.body.title);
    if (!req.body.title) {
        return res.status(400).send({ message: 'title is required or malformed' });
    }

    // check Filename is valid
    if (!req.body.content) {
        return res.status(400).send({ message: 'content is required or malformed' });
    }
    //add post first
    const post = await new Posts({
        title: req.body.title,
        content: req.body.content,
        ///url: fileName
    });

    const saved_post = await post.save();

    //add media files and reference to post
    const media = await new Medias({
        url: req.body.filename,
        postId: saved_post.id,
    });

    const saved_media = await media.save();
    saved_media.url = AWS.getGetSignedUrl(saved_media.url);
    res.status(201).send(saved_post);
};