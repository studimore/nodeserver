import { Table, Column, Model, BelongsTo, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Posts } from './Posts';

@Table
export class Comments extends Model<Comments> {
  @Column
  content?: string;

  @Column
  url!: string;

  //could reference Profile Table
  @Column
  authorId!: number;

  @ForeignKey(() => Posts)
  @Column
  postId!: number;

  @BelongsTo(() => Posts)
  post!: Posts;

  @Column
  @CreatedAt
  createdAt!: Date;

  @Column
  @UpdatedAt
  updatedAt!: Date;
}
