import { Table, Column, Model, HasMany, PrimaryKey, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Medias } from './Medias';

@Table
export class MediaTypes extends Model<MediaTypes> {
  @Column
  typeName!: string;

  @HasMany(() => Medias)
  media: Medias[];
}
