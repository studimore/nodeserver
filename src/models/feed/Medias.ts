import { Table, Model, Column, HasMany, PrimaryKey, CreatedAt, UpdatedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Posts } from './Posts';
import { MediaTypes } from './MediaTypes';

@Table
export class Medias extends Model<Medias> {
  @Column
  url!: string;

  @ForeignKey(() => Posts)
  @Column
  postId!: number;

  @BelongsTo(() => Posts)
  post: Posts;

  @ForeignKey(() => MediaTypes)
  @Column
  typeId!: number;

  @BelongsTo(() => MediaTypes)
  type!: MediaTypes;

  @Column
  @CreatedAt
  public createdAt: Date = new Date();

  @Column
  @UpdatedAt
  public updatedAt: Date = new Date();
}
