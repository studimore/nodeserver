import { Table, Model, Column, HasMany, PrimaryKey, CreatedAt, UpdatedAt, ForeignKey } from 'sequelize-typescript';
import { Comments } from "./Comments";

@Table
export class Posts extends Model<Posts> {
  @Column
  tag: string;

  @Column
  title: string;

  @Column
  content: string;

  // could reference Profile Table
  @Column
  authorId!: number;

  @HasMany(() => Comments)
  comments: Comments[];

  @Column
  @CreatedAt
  public createdAt: Date = new Date();

  @Column
  @UpdatedAt
  public updatedAt: Date = new Date();
}
