//feed models
import { Comments } from './feed/Comments';
import { FeedItems } from './feed/FeedItems';
import { Medias } from './feed/Medias';
import { MediaTypes } from './feed/MediaTypes';
import { Posts } from './feed/Posts';

//user Models
import { Accounts } from './user/Accounts';
import { Communities } from './user/Communities';
import { Managers } from './user/Managers';
import { Profiles } from './user/Profiles';
import { Registrations } from './user/Registrations';
import { Students } from './user/Students';
import { Users } from './user/Users';

export const V0MODELS = [ Comments, FeedItems, Medias, Posts, MediaTypes, Users, Accounts, Communities, Managers, Profiles, Registrations, Students];
