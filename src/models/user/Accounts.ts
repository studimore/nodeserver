import {Table, Column, Model, HasMany, PrimaryKey, CreatedAt, UpdatedAt, HasOne} from 'sequelize-typescript';
import { Profiles } from './Profiles';

@Table
export class Accounts extends Model<Accounts> {

  @Column
  email!: string;

  @Column
  phone!: string;

  @Column
  password_hash!: string;

  @HasOne (()=> Profiles)
  profile?: Profiles;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
