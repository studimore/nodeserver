import {Table, Column, Model, ForeignKey, CreatedAt, UpdatedAt, HasOne, BelongsToMany, HasMany} from 'sequelize-typescript';
import { Profiles } from './Profiles';
import { Managers } from './Managers';

@Table
export class Communities extends Model<Communities> {
  @Column
  communityName!: string;

  @Column
  domain!: string;

  @HasOne(()=> Profiles)
  profile?: Profiles;

  @HasMany(()=> Managers)
  managers?: Managers[]
  
}
