import {Table, Column, Model, ForeignKey, CreatedAt, UpdatedAt, HasOne, BelongsToMany} from 'sequelize-typescript';
import { Profiles } from './Profiles';
import { Communities } from './Communities';

@Table
export class Managers extends Model<Managers> {
  @Column
  department!: string;

  @HasOne(()=> Profiles)
  profile!: Profiles;

  @ForeignKey(() => Communities)
  @Column
  communityId: number | null;
}
