import {Table, Column, Model, HasMany, ForeignKey, CreatedAt, UpdatedAt} from 'sequelize-typescript';
import {Accounts} from "./Accounts";
import { Communities } from './Communities';
import { Managers } from './Managers';
import { Students } from './Students';

@Table
export class Profiles extends Model<Profiles> {
  @Column
  firstName!: string;

  @Column
  lastName!: string;
  
  @Column
  birthday?: Date;

  @ForeignKey(() => Accounts)
  @Column
  accountId!: number;

  @ForeignKey(() => Students)
  @Column
  studentId: number | null;

  @ForeignKey(() => Managers)
  @Column
  managerId: number | null;

  @ForeignKey(() => Communities)
  @Column
  communityId: number | null;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
