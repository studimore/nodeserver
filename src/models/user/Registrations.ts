import {Table, Column, Model , CreatedAt, UpdatedAt, HasOne, PrimaryKey} from 'sequelize-typescript';

@Table
export class Registrations extends Model<Registrations> {

  @PrimaryKey
  @Column
  email!: string;

  @Column
  phone!: number;

  @Column
  isPhoneVerified: boolean | false ; 

  @Column
  isEmailVerified: boolean | false; 

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
