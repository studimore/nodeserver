import {Table, Column, Model, ForeignKey, CreatedAt, UpdatedAt, HasOne} from 'sequelize-typescript';
import { Profiles } from './Profiles';

@Table
export class Students extends Model<Students> {
  @Column
  classStanding!: string;

  @Column
  housing!: string;
  
  @Column
  major!: string;

  @HasOne(()=> Profiles)
  profile!: Profiles;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
