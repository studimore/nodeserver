import { Router, Request, Response } from 'express';

const router: Router = Router();
const feedController = require('../../controllers/feed.controller');

router.get('/', feedController.getAllFeedItems);

router.get('/:id', feedController.getFeedItem);

router.post('/', feedController.createFeed);

export const FeedRouter: Router = router;


