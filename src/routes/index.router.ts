import { Router, Request, Response } from 'express';
import { UserRouter } from './user/user.router';
import { FeedRouter } from './feed/feed.router';

const router: Router = Router();

router.use('/user', UserRouter);

router.use('/feed', FeedRouter);

router.get('/', async (req: Request, res: Response) => {
    res.send(`V0`);
});

export const IndexRouter: Router = router;