import { Router, Request, Response } from 'express';

const router: Router = Router();
const authController = require('../../controllers/auth.controller');

router.get('/', authController.authRoute);

router.get('/verification', authController.getVerification);

router.post('/', authController.createUser);

router.post('/login', authController.loginUser);

export const AuthRouter: Router = router;

